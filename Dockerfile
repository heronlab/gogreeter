FROM golang:1.13-alpine as builder

WORKDIR /src
RUN apk --no-cache add build-base git gcc

ADD . /src
RUN go mod download \
    && go build -o app

FROM alpine:3.10
WORKDIR /
COPY --from=builder /src/app /app

ENTRYPOINT ["/app"]
