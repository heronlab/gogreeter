package main

import (
	"flag"
	"fmt"
	"net/http"
	"net/http/httputil"
)

var (
	listenAddr  string
	message     string
	dumpRequest bool
)

func handle(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte(message))

	if dumpRequest {
		raw, err := httputil.DumpRequest(r, true)
		if err != nil {
			fmt.Printf("failed to dump request: %v", err)
		} else {
			fmt.Println(string(raw))
		}
	}
}

func main() {
	flag.StringVar(&listenAddr, "listen", ":3000", "address to listen, example :3000")
	flag.StringVar(&message, "message", "hello world!", "message to response, example: hello world!")
	flag.BoolVar(&dumpRequest, "dump-request", false, "dump request detail to stdout, default: false")
	flag.Parse()

	http.HandleFunc("/", handle)
	fmt.Printf("listening on %s\n", listenAddr)
	http.ListenAndServe(listenAddr, nil)
}
