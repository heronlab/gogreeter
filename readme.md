# gogreeter

This is a simple http server for testing echo.
You just need to install it via command `go install gitlab.com/phuonghuynhnet/gogreeter` and start a testing server:

```
gogreeter -listen=":2900" -message="hello world!"
```

try to test via curl

```
curl localhost:2900
```

you can simple get message `hello world!` in response.

